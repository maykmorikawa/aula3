<div class="media">
  <div class="media-left media-middle">
    <a href="#">
      <img class="media-object" src="https://loremflickr.com/350/200/brazil,rio" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">Middle aligned media</h4>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras in commodo lorem. Praesent condimentum tellus id hendrerit egestas. Vivamus suscipit nisl non elit lacinia posuere.
  </div>
</div>
<div class="media">
  <div class="media-left media-middle">
    <a href="#">
      <img class="media-object" src="https://loremflickr.com/350/200/paris" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">Middle aligned media</h4>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras in commodo lorem. Praesent condimentum tellus id hendrerit egestas. Vivamus suscipit nisl non elit lacinia posuere.
  </div>
</div>
<div class="media">
  <div class="media-left media-middle">
    <a href="#">
      <img class="media-object" src="https://loremflickr.com/350/200/" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">Middle aligned media</h4>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras in commodo lorem. Praesent condimentum tellus id hendrerit egestas. Vivamus suscipit nisl non elit lacinia posuere.
  </div>
</div>
